#v1.1
#2019-09-14, ICE 739 nach Muenchen
#2019-09-16 ICE 800 nach Erfurt

TRIP=0;
ERRS=0;

if [ -z $1 ]
then 
        echo "ice.sh (ZUGNUMMER|UNIQUE) PATH"
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ] 
then
        echo "ice.sh (ZUGNUMMER|UNIQUE) PATH"
else
        while(true)
        do      
                
                DAT=$(curl -k -s https://iceportal.de/api1/rs/status) 
                
                if [ $? -ne 0 ]; then
                        
                        ERRS=$(($ERRS+1));
                        
                        if [ $ERRS -gt 3 ]
                        then
                                exit "No Connection"
                        fi

                else
                        ERRS=0
                fi
                
                echo $DAT >> $2ice$1.status
                echo -e "\r\n" >> $2ice$1.staus

                if [ $TRIP == 6 ] 
                then
                        DAT=$(curl -k -s https://iceportal.de/api1/rs/tripInfo/trip)
                        echo $DAT >> $2ice$1.trip
                        echo -e "\r\n" >> $2ice$1.trip
                        TRIP=1
                fi
                echo $TRIP;
                TRIP=$(($TRIP+1));
                sleep 10;
        done
fi